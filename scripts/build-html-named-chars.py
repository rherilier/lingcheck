#!/usr/bin/env python

import json
import os
import requests

file = "entities.json"
url = "https://html.spec.whatwg.org/" + file

content=""

if not os.path.isfile(file):
    r = requests.get(url)
    with open(file, mode="wb") as f:
        f.write(r.content)
        content = r.content
else:
    with open(file, "rb") as f:
        content = f.read()

data = json.loads(content)

specials = {
    "\n": "\\n",
    '"': "\\\"",
    '\\': "\\\\"
}

print("\n#include <map>")

print("\n/* extracted from the JSon file provided at from https://html.spec.whatwg.org/multipage/named-characters.html")
print(" */")
print("std::map<const char*, const char*> map = {")

for k in data.keys():
    if not k.endswith(";"):
        continue

    s = data[k]['characters']
    if s in specials:
        s = specials[s]

    print('  { "%s", "%s" },' % (k, s))

print("};\n")

