/* Copyright © 2024 Rémi Hérilier
 *
 * This file is part of the Linguistic Check project.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#ifndef LINGCHECK_WTE_FR_EXTRACTOR_H
#define LINGCHECK_WTE_FR_EXTRACTOR_H

#include <lingcheck/wte/common/extractor.h>

namespace lingcheck::wte::fr {

class extractor : public common::extractor
{
public:
    extractor()
    {}

public:
    void extract(const tokens& input, tokens& output) override;
};

} // namespace lingcheck::wte::fr

#endif // LINGCHECK_WTE_FR_EXTRACTOR_H
