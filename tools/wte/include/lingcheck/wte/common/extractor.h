/* Copyright © 2024 Rémi Hérilier
 *
 * This file is part of the Linguistic Check project.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#ifndef LINGCHECK_WTE_COMMON_EXTRACTOR_H
#define LINGCHECK_WTE_COMMON_EXTRACTOR_H

#include <lingcheck/tokens.h>

#include <lingcheck/format/mediawiki.h>

#include <memory>

namespace lingcheck::wte::common {

class extractor
{
public:
    extractor()
        : _format(new mediawiki)
    {}

    virtual ~extractor() = default;

public:
    const lingcheck::format* format() const { return _format.get(); }

public:
    virtual void extract(const tokens& input, tokens& output) = 0;

private:
    std::unique_ptr<lingcheck::format> _format;
};

} // namespace lingcheck::wte::common

#endif // LINGCHECK_WTE_COMMON_EXTRACTOR_H
