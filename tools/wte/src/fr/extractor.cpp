/* Copyright © 2024 Rémi Hérilier
 *
 * This file is part of the Linguistic Check project.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <lingcheck/wte/fr/extractor.h>

#include <pcrecpp.h>

/*****************************************************************************
 * some regexp
 *****************************************************************************/

static pcrecpp::RE images_re("^(File|Fichier|Image)", pcrecpp::RE_Options(PCRE_CASELESS));

/*****************************************************************************
 * or helpers
 *****************************************************************************/

inline bool is_valid_lang(const lingcheck::token& t)
{
    return (t == "fr") || (t == "conv");
}

/*****************************************************************************
 * lingcheck::wte::french_extractor::extract(const tokens&, tokens&)
 *****************************************************************************/

void lingcheck::wte::fr::extractor::extract(const lingcheck::tokens& tokens, lingcheck::tokens& output)
{
    size_t i = 0;
    lingcheck::tokens props;
    bool pass = false;
    bool in_wanted_h2 = false;

    while (i < tokens.size()) {
        const auto& token = tokens[i];
        lingcheck::TYPE type = token.type();

        if (pass == false) {
            if ((type != lingcheck::H2) && (type != lingcheck::H3)) {
                ++i;
                continue;
            }

            size_t p = i;

            ++i;

            if (lingcheck::move_while(tokens, i, lingcheck::SPACE) == false) {
                continue;
            }

            if (tokens[i].is(lingcheck::TEMPLATE) == false) {
                lingcheck::move_after(tokens, i, lingcheck::RETURN);
                continue;
            }

            format()->get_props(tokens[i], props);

            if (type == lingcheck::H2) {
                in_wanted_h2 = (props.size() == 2) && (props[0] == "langue") && is_valid_lang(props[1]);

                if (in_wanted_h2 == false) {
                    lingcheck::move_after(tokens, i, lingcheck::RETURN);
                    continue;
                }
            } else if ((in_wanted_h2 == false) || (type != lingcheck::H3)) {
                lingcheck::move_after(tokens, i, lingcheck::RETURN);
                continue;
            } else if ((props.size() >= 3) && (props[0] == "S") && is_valid_lang(props[2])) {
                pass = true;
            } else if ((props.size() == 2) && (props[0] == "S") && (props[1] == "étymologie")) {
                pass = true;
            } else {
                lingcheck::move_after(tokens, i, lingcheck::RETURN);
                continue;
            }

            if (lingcheck::move_after(tokens, i, lingcheck::RETURN) == false) {
                continue;
            }

            // do not forget to copy the whole title block
            while (p < i) {
                output.push_back(tokens[p]);
                ++p;
            }

            continue;
        }

        /* finding a title indicate we can search for a new useful title
         */
        if (token.is_title()) {
            pass = false;
            continue;
        }

        /* no need for styles
         */
        if (token.is_style()) {
            ++i;
            continue;
        }

        /* or scripts
         */
        if (token.is_script()) {
            ++i;
            continue;
        }

        /* no markup blocks
         */
        if (token.is(lingcheck::MARKUP_OPEN)) {
            lingcheck::move_after(tokens, i, lingcheck::MARKUP_CLOSE);
            continue;
        }

        /* and no other extras
         */
        if (token.is_extra()) {
            ++i;
            continue;
        }

        /* nor hrules
         */
        if (token.is(lingcheck::HRULE)) {
            ++i;
            continue;
        }

        /* nor tables
         */
        if (token.is(lingcheck::TABLE_OPEN)) {
            lingcheck::move_after(tokens, i, lingcheck::TABLE_CLOSE);
            continue;
        }

        /* or examples (in the way of writing pages for the french wiktionary)
         */
        if (token.is_list() && (token.size() > 1)) {
            // we need to search for the list entry's end
            do {
                if (lingcheck::move_after(tokens, i, lingcheck::is_nl) == false) {
                    break;
                }

                if (tokens[i].is_list() || tokens[i].is_title() || tokens[i].is_nl()) {
                    break;
                }
            } while (true);
            continue;
        }

        if (token.is_reference()) {
            if (token.is(lingcheck::INLINK)) {
                /* remove illustrative images
                 */
                format()->get_props(token, props);

                if (images_re.PartialMatch(pcrecpp::StringPiece(props[0].data(), props[0].size()))) {
                    lingcheck::move_after(tokens, i, lingcheck::RETURN);
                    continue;
                }

                /* but clean others
                 */
                if (props.size() == 1) {
                    output.push_back(props[0]);
                } else {
                    output.push_back(props[1]);
                }
            } else if (token.is(lingcheck::TEMPLATE)) {
                /* template contains what we want (or not)
                 */
                format()->get_props(token, props);

                if (props[0] == "illustration") {
                    lingcheck::move_after(tokens, i, lingcheck::RETURN);
                    continue;
                }

                if ((props[0] == "illustration texte")) {
                    ++i;
                    continue;
                }

                output.push_back(token);
            }

            ++i;
            continue;
        }

        /* remaining tokens can be dumped as they are
         */
        output.push_back(token);
        ++i;
    }
}
