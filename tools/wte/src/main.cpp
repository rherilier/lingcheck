/* Copyright © 2024 Rémi Hérilier
 *
 * This file is part of the Linguistic Check project.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <lingcheck/tokens.h>
#include <lingcheck/utils.h>

#include <lingcheck/format/mediawiki.h>

#include <lingcheck/wte/fr/extractor.h>

#include <cstdio>
#include <cstring>

#include <libxml/parser.h>
#include <libxml/SAX.h>

#include <unistd.h>

/*****************************************************************************
 * a context for SAX
 *****************************************************************************/

class sax_ctx
{
public:
    enum TYPE : int {
        WHATEVER,
        TITLE,
        TEXT
    };

public:
    sax_ctx(FILE* out, lingcheck::wte::common::extractor* extractor)
        : _format(new lingcheck::mediawiki),
          _extractor(extractor),
          _out(out)
    {}

public:
    TYPE type() const { return _type; }
    void set_type(TYPE type) { _type = type; }

public:
    std::string& word() { return _word; }
    std::string& text() { return _text; }

public:
    lingcheck::tokens& tokens() { return _tokens; }
    lingcheck::tokens& result() { return _result; }

public:
    FILE* out() { return _out; }

public:
    void tokenize(std::string &text, lingcheck::tokens &tokens)
    {
        _format->tokenize(text, tokens);
    }

    void extract(const lingcheck::tokens& tokens, lingcheck::tokens& result)
    {
        _extractor->extract(tokens, result);
    }

private:
    lingcheck::tokens _tokens;
    lingcheck::tokens _result;

    std::string _word;
    std::string _text;

    std::unique_ptr<lingcheck::mediawiki> _format;
    std::unique_ptr<lingcheck::wte::common::extractor> _extractor;
    FILE* _out;
    TYPE _type;
};

/*****************************************************************************
 * XML stats
 *****************************************************************************/

unsigned all_count = 0;
unsigned namespace_count = 0;
unsigned prefix_count = 0;
unsigned suffix_count = 0;
unsigned word_count = 0;
unsigned invalid_count = 0;

/*****************************************************************************
 * XML parsing
 *****************************************************************************/

void start_element(void* user_data, const xmlChar* name, const xmlChar**)
{
    auto* ctx = static_cast<sax_ctx*>(user_data);

    if (::strcmp(reinterpret_cast<const char*>(name), "title") == 0) {
        ctx->set_type(sax_ctx::TITLE);
        ctx->word().clear();
    } else if (::strcmp(reinterpret_cast<const char*>(name), "text") == 0) {
        ctx->set_type(sax_ctx::TEXT);
        ctx->text().clear();
    } else {
        ctx->set_type(sax_ctx::WHATEVER);
    }
}

void end_element(void* user_data, const xmlChar* name)
{
    if (::strcmp(reinterpret_cast<const char*>(name), "page") != 0) {
        return;
    }

    auto* ctx = static_cast<sax_ctx*>(user_data);
    auto& word = ctx->word();

    lingcheck::utils::trim(word);

    ++all_count;

    if (::memchr(word.c_str(), ':', word.length()) != nullptr) {
        ++namespace_count;
        return;
    }

    if (word.back() ==  '-') {
        ++prefix_count;
        return;
    }

    static const char* interpunct = "·";
    static const size_t interpunct_len = strlen(interpunct);

    if ((word.size() > 1) &&
        ((word[0] ==  '-')
         || (std::memcmp(word.c_str(), interpunct, interpunct_len) == 0))) {
        ++suffix_count;
        return;
    }

    auto& text = ctx->text();
    auto& tokens = ctx->tokens();
    auto& result = ctx->result();

    lingcheck::utils::trim(text);

    tokens.clear();
    result.clear();

    ctx->tokenize(text, tokens);
    ctx->extract(tokens, result);

    if (result.size() == 0) {
        ++invalid_count;
        return;
    }

    ++word_count;

    /* use a fake template to ease catching not-a-single-word entries
     */
    fprintf(ctx->out(), "= {{T|%s}} =\n", word.data());

    size_t i = 0;

    while (i < result.size()) {
        const auto& t = result[i];

        if (t.is_nl()) {
            fprintf(ctx->out(), "\n");
        } else {
            fprintf(ctx->out(), "%.*s", t.size(), t.data());
        }

        ++i;

        if (t.is_nl()) {
            lingcheck::move_while(result, i, lingcheck::is_nl);
        }
    }

    /* make sure to have 2 nl after each word (just for readability sake when debugging)
     */
    for (unsigned i = lingcheck::is_nl(result.back()); i < 2; ++i) {
        fputs("\n", ctx->out());
    }
}

void characters(void* user_data, const xmlChar *data, int len)
{
    auto* ctx = static_cast<sax_ctx*>(user_data);
    sax_ctx::TYPE type = ctx->type();

    if (type == sax_ctx::TITLE) {
        ctx->word() += std::string_view(reinterpret_cast<const char*>(data), len);
    } else if (type == sax_ctx::TEXT) {
        ctx->text() += std::string_view(reinterpret_cast<const char*>(data), len);
    }
}

/*****************************************************************************
 * main()
 *****************************************************************************/

int main(int argc, char** argv)
{
    const char* name;
    FILE* in;
    FILE* out;
    int c;
    std::string lang = "fr";

    while ((c = getopt (argc, argv, "l:")) != -1) {
        switch (c)
        {
        case 'l':
            lang = optarg;
            break;
        case '?':
            if (optopt == 'l') {
                fprintf (stderr, "option -l requires an argument.\n");
            } else {
                fprintf (stderr, "unknown option `-%c'.\n", optopt);
            }
            return 1;
        default:
            return 1;
        }
    }

    std::unique_ptr<lingcheck::wte::common::extractor> extractor;

    if (lang == "fr") {
        extractor.reset(new lingcheck::wte::fr::extractor);
    } else {
        fprintf(stderr, "'%s' is not a valid language, supported ones are: fr\n",
                lang.c_str());
        return 1;
    }

    printf("using '%s' as input language\n", lang.c_str());

    int narg = (argc + 1) - optind;

    if (narg == 1) {
        name = "stdin";
        in = stdin;
        out = stdout;
    } else if (narg == 2) {
        name = argv[optind];
        in = fopen(argv[optind], "rb");
        out = stdout;
    } else {
        name = argv[optind];
        in = fopen(argv[optind], "rb");
        out = fopen(argv[optind + 1], "wb");
    }

    if (in == nullptr) {
        return 1;
    }

    if (out == nullptr) {
        return 1;
    }

    static constexpr size_t buffer_size = 1024 * 1024;
    std::unique_ptr<char> buffer(new char [buffer_size]);
    size_t res = ::fread(buffer.get(), 1, buffer_size, in);
    sax_ctx ctx(out, extractor.release());
    xmlSAXHandler handler = {};

    handler.startElement = start_element;
    handler.endElement = end_element;
    handler.characters = characters;

    auto xml_stream = xmlCreatePushParserCtxt(&handler, &ctx,
                                              buffer.get(), res,
                                              name);

    while ((res = ::fread(buffer.get(), 1, buffer_size, in)) > 0) {
        xmlParseChunk(xml_stream, buffer.get(), res, 0);
    }

    xmlParseChunk(xml_stream, buffer.get(), 0, 1);
    xmlFreeParserCtxt(xml_stream);

    if (in != stdin) {
        fclose(in);
    }

    if (out != stdout) {
        fclose(out);
    }

    printf("meta    : %u\n", namespace_count);
    printf("invalids: %u\n", invalid_count);
    printf("words   : %u\n", word_count);
    printf("prefixes: %u\n", prefix_count);
    printf("suffixes: %u\n", suffix_count);
    printf("all     : %u\n", all_count);

    return 0;
}
