# Linguistic Check

Linguistic Checker, lingcheck for short, is actually more a proof of concept than a real project.

It aims to be a framework to create orthographic and grammar checker using Wiktionary's databases for the lexical part.

The first targeted language is french (which is a little bit more complex than english but lesser than german:).

## License

This software is distributed under the GNU Affero General Public License version 3 or later (see LICENSE for details).

## Tools

`tools/wte` to parse and extract MW relevant information from Wiktionary dumps taken from https://dumps.wikimedia.org/

## Requirements

* a C++ 17 compliant compiler;
* flex .

## Contribute

1. Fork;
2. Write code;
3. Send a merge request.
