/* Copyright © 2024 Rémi Hérilier
 *
 * This file is part of the Linguistic Checker project.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <lingcheck/format/mediawiki.h>

struct test_t {
    const lingcheck::token input;
    std::vector<lingcheck::token> expected;
};

int main()
{
    std::vector<test_t> tests = {
        {
            "{{S|t}}",
            { "S", "t" }
        },
        {
            "{{lexique|chimie|fr}}",
            { "lexique", "chimie", "fr" }
        },
        {
            "[[Image: aa.jpg|AA]]",
            { "Image: aa.jpg", "AA" }
        },
        {
            "[[Image: aa.jpg|AA{{e}}]]",
            { "Image: aa.jpg", "AA{{e}}" }
        },
        {
            "{{S|AA{{e}}}}",
            { "S", "AA{{e}}" }
        },
        {
            "{{fchim|[{{fchim|O||-Si||({{fchim|C||H|3}})|2}}]|n}}",
            { "fchim", "[{{fchim|O||-Si||({{fchim|C||H|3}})|2}}]", "n" }
        }
    };

    lingcheck::mediawiki mw;
    lingcheck::tokens props;
    int res = 0;

    for (const auto& test : tests) {
        const auto& input = test.input;
        const auto& expected = test.expected;

        mw.get_props(input, props);

        if (props.size() != expected.size()) {
            ++res;
            continue;
        }

        for (size_t i = 0; i < expected.size(); ++i) {
            res += props[i] != expected[i];
        }
    }

    return res;
}
