/*
 * This file is part of the Linguistic Checker project.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <lingcheck/token.h>

const char* lingcheck::token::type_name() const noexcept
{
    return type_name(type());
}

const char* lingcheck::token::type_name(TYPE type) noexcept
{
    switch (type) {
    case IGNORED:
        return "ignored";
        break;
    case UNDEFINED:
        return "undefined";
        break;
    case RETURN:
        return "nl";
        break;
    case SPACE:
        return "space";
        break;
    case PUNCTUATION:
        return "punctuation";
        break;
    case DASH:
        return "dash";
        break;
    case SQUOTE:
        return "single quote";
        break;
    case DQUOTE:
        return "double quote";
        break;
    case BRACKET:
        return "bracket";
        break;
    case SYMBOL:
        return "symbol";
        break;
    case WORD:
        return "word";
        break;
    case TEMPLATE:
        return "template";
        break;
    case INLINK:
        return "in-link";
        break;
    case OUTLINK:
        return "out-link";
        break;
    case URL:
        return "url";
        break;
    case MARKUP:
        return "markup";
        break;
    case MARKUP_OPEN:
        return "markup-open";
        break;
    case MARKUP_CLOSE:
        return "markup-close";
        break;
    case COMMENT:
        return "comment";
        break;
    case ITALIC:
        return "italic";
        break;
    case BOLD:
        return "bold";
        break;
    case BOLD_ITALIC:
        return "bold-italic";
        break;
    case STRIKETHROUGH:
        return "strikethrough";
        break;
    case LIST:
        return "list";
        break;
    case HRULE:
        return "hrule";
        break;
    case TAG:
        return "table";
        break;
    case H1:
        return "h1";
        break;
    case H2:
        return "h2";
        break;
    case H3:
        return "h3";
        break;
    case H4:
        return "h4";
        break;
    case H5:
        return "h5";
        break;
    case H6:
        return "h6";
        break;
    default:
        return "unknown";
        break;
    }
}
