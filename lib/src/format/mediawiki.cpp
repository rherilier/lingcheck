/* Copyright © 2024 Rémi Hérilier
 *
 * This file is part of the Linguistic Checker project.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <lingcheck/format/mediawiki.h>

#include <mw_scanner.h>

void lingcheck::mediawiki::tokenize(std::string& text, tokens& tokens) const
{
    yyscan_t scanner;

    mwlex_init(&scanner);
    mwlex_init_extra(&tokens, &scanner);

    // make sure to have them explicitly in [0, size)
    text.append(2, '\0');

    auto state = mw_scan_buffer(text.data(), text.size(), scanner);

    mwlex(scanner);
    mw_delete_buffer(state, scanner);
    mwlex_destroy(scanner);
}

size_t lingcheck::mediawiki::get_props(const token& token, tokens& props) const
{
    const int n = token.size() - 2;
    int pos = 2;
    int i = 2;
    int nested = 0;

    props.clear();

    while (i < n) {
        if (token[i] == '{') {
            ++i;

            if ((i < n) && (token[i] == '{')) {
                ++i;
                ++nested;
            }

            continue;
        } else if (token[i] == '}') {
            ++i;

            if ((i < n) && (token[i] == '}')) {
                ++i;
                --nested;
            }

            continue;
        }

        if ((nested == 0) && (token[i] == '|')) {
            props.emplace_back(token.data() + pos, i - pos);
            pos = i + 1;
        }

        ++i;
    }

    props.emplace_back(token.data() + pos, i - pos);

    return props.size();
}
