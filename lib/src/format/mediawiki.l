
RETURN          ("\r\n"|"\n")
SPACE           ("\t"|" "|" "|" "|" "|" "|" "|" "|" "|" "|" "|" "|"␢"|"␣"|"⍽")
NUMBER          [0-9]+
PUNCT           (","|";"|"."|"…"|":"|"!"|"?"|"¡"|"¿"|"/"|"\\"|"⸳"|"‽"|"⸮")
DASH            ("-"|"-"|"‐"|"‑"|"‒"|"–"|"—"|"⸺"|"⸻"|"―"|"⁃"|"−"|"─")
SQUOTE          ("'"|"ʼ"|"‘"|"‛"|"´"|"`"|"′"|"‵"|"՚"|"ꞌ"|"Ꞌ")
DQUOTE          ("\""|"«"|"»"|"“"|"”")
BRACKET         ("{"|"}"|"("|")"|"["|"]"|"⸢"|"⸣"|"⸤"|"⸥"|"<"|">"|"‹"|"›"|"〈"|"〉"|"⟨"|"⟩"|"〈"|"〉"|"⸌"|"⸍"|"⌊"|"⌋"|"⌈"|"⌉"|"⌜"|"⌝"|"⌞"|"⌟")
SYMBOL          ("%"|"⁒"|"@"|"*"|"⁂"|"|"|"¦"|"ː"|"©"|"℗"|"®"|"🅫"|"™"|"🅪"|"℠"|"✓"|"#"|"⁜"|"※"|"°"|"№"|"⁊"|"†"|"‡"|"′"|"″"|"‴"|"§"|"∴"|"∵"|"¶"|"•"|"_"|"◼"|"✘"|"ɔ:"|"ɔ"|"+"|"−"|"±"|"×"|"÷"|"="|"≠"|"<"|"≤"|">"|"≥"|"≮"|"≯"|"≈"|"⁄ "|"/"|"‰"|"‱"|"√"|"∫"|"∑"|"∏"|"µ"|"∧"|"∨"|"∃"|"∀"|"¬"|"∅"|"∩"|"∪"|"⊆"|"⊇"|"∈"|"∋")
MONETARY        ("¤"|"฿"|"₿"|"₵"|"¢"|"₡"|"$"|"₫"|"֏"|"€"|"₲"|"₴"|"₭"|"£"|"₺"|"₼"|"₦"|"₱"|"៛"|"₽"|"₹"|"₪"|"₸"|"₮"|"₩"|"¥")
HTML_CHAR_NAMED "&"[a-zA-Z]+";"
HTML_CHAR_DEC   "&#"[0-9]+";"
HTML_CHAR_HEX   "&#"[xX][0-9a-fA-F]+";"
HTML_TAGS       ("abbr"|"blockquote"|"code"|"del"|"div"|"ins"|"pre"[^>]*|"q"|"ref"|"span")
HTML_STYLES     ("big"|"chem"|"em"|"hiero"|"i"|"s"|"small"|"strong"|"u")
HTML_SCRIPTS    ("sub"|"sup")
MW_TAGS         ("gallery"|"math"|"noinclude"|"nowiki"|"ref")
TAGS            ({HTML_TAGS}|{MW_TAGS})
TAG_OPEN        "<"([ \n]*){TAGS}([ \n]*)">"
TAG_CLOSE       "</"([ \n]*)({TAGS})([ \n]*)">"
TAG_SOLO        "<"(("br"|"nowiki")([ \n]+)("/"?([ \n]*))|"li "+[^>]+)">"
STYLE_OPEN      "<"([ \n]*){HTML_STYLES}([ \n]*)">"
STYLE_CLOSE     "</"([ \n]*){HTML_STYLES}([ \n]*)">"
SCRIPT_CLOSE    "</"([ \n]*){HTML_SCRIPTS}([ \n]*)">"
SCRIPT_OPEN     "<"([ \n]*){HTML_SCRIPTS}([ \n]*)">"
VALIDURLCHARS   ([A-Za-z0-9]|"-"|"."|"_"|"~"|":"|"/"|"?"|"#"|"["|"]"|"@"|"!"|"$"|"&"|"'"|"*"|"+"|","|";"|"%"|"=")
URL             ([a-z]+)"://"({VALIDURLCHARS}+)
TITLEDLINK      "["({URL})" "[^\]]*"]"
WIKILINK_OPEN   "[["
WIKILINK_CLOSE  "]]"
TEMPLATE_OPEN   "{{"
TEMPLATE_CLOSE  "}}"
COMMENT_OPEN    "<!--"
COMMENT_CLOSE   "-->"
LIST            ^(":"|"#"|";"|"*")+
TABLE_OPEN      "{|"
TABLE_CLOSE     "|}"
TABLE_CAPTION   "|+"
TABLE_ROW       "|-"
TABLE_HEADER    ("!"|"!!")
TABLE_DATA      ("|"|"||")
HR              ^"----"
H1              ^"="
H2              ^"=="
H3              ^"==="
H4              ^"===="
H5              ^"====="
H6              ^"======"
ITALIC          "''"
BOLD            "'''"
BOLD_ITALIC     "''''''"

%option caseless stack
%option noyywrap nounput
%option noyy_push_state noyy_pop_state noyy_top_state
%option reentrant
%option extra-type="lingcheck::tokens*"
%x WIKILINK_COND TEMPLATE_COND COMMENT_COND

%{
#include <lingcheck/tokens.h>

#define TOKEN_APPEND3(TEXT, L, T) yyextra->emplace_back((TEXT), (L), lingcheck::T);
#define TOKEN_APPEND(T) TOKEN_APPEND3(yytext, yyleng, T)

static char* lexer_text = nullptr;
static int lexer_leng = 0;

#define LEXER_ACCUM()                                           \
    if ((lexer_text == nullptr) && (lexer_leng == 0)) {         \
        lexer_text = yytext;                                    \
    }                                                           \
                                                                \
    ++lexer_leng;

#define LEXER_FLUSH()                                           \
    if ((lexer_text != nullptr) && (lexer_leng != 0)) {         \
        TOKEN_APPEND3(lexer_text, lexer_leng, WORD);            \
    }                                                           \
                                                                \
    lexer_text = nullptr;                                       \
    lexer_leng = 0;

static char* block_text = nullptr;
static int block_leng = 0;
static int block_level = 0;

#define BLOCK_INIT()                            \
    block_text = yytext;                        \
    block_leng = yyleng;                        \
    block_level = 1;

#define BLOCK_ENTER()                           \
    ++block_level;                              \
    block_leng += yyleng;

#define BLOCK_LEAVE(WHAT)                               \
    --block_level;                                      \
    block_leng += yyleng;                               \
                                                        \
    if (block_level == 0) {                             \
        TOKEN_APPEND3(block_text, block_leng, WHAT);    \
        BEGIN(INITIAL);                                 \
    }

#define BLOCK_CONTINUE()                        \
    block_leng += yyleng;

%}

%%

{BOLD_ITALIC}     { LEXER_FLUSH(); TOKEN_APPEND(BOLD_ITALIC); }
{BOLD}            { LEXER_FLUSH(); TOKEN_APPEND(BOLD); }
{ITALIC}          { LEXER_FLUSH(); TOKEN_APPEND(ITALIC); }
{STYLE_OPEN}      { LEXER_FLUSH(); TOKEN_APPEND(STYLE_OPEN); }
{STYLE_CLOSE}     { LEXER_FLUSH(); TOKEN_APPEND(STYLE_CLOSE); }
{SCRIPT_OPEN}     { LEXER_FLUSH(); TOKEN_APPEND(SCRIPT_OPEN); }
{SCRIPT_CLOSE}    { LEXER_FLUSH(); TOKEN_APPEND(SCRIPT_CLOSE); }

{H6}              { LEXER_FLUSH(); TOKEN_APPEND(H6); }
{H5}              { LEXER_FLUSH(); TOKEN_APPEND(H5); }
{H4}              { LEXER_FLUSH(); TOKEN_APPEND(H4); }
{H3}              { LEXER_FLUSH(); TOKEN_APPEND(H3); }
{H2}              { LEXER_FLUSH(); TOKEN_APPEND(H2); }
{H1}              { LEXER_FLUSH(); TOKEN_APPEND(H1); }

{HR}              { LEXER_FLUSH(); TOKEN_APPEND(HRULE); }
{LIST}            { LEXER_FLUSH(); TOKEN_APPEND(LIST); }

{TABLE_OPEN}      { LEXER_FLUSH(); TOKEN_APPEND(TABLE_OPEN); }
{TABLE_CLOSE}     { LEXER_FLUSH(); TOKEN_APPEND(TABLE_CLOSE); }
{TABLE_CAPTION}   { LEXER_FLUSH(); TOKEN_APPEND(TABLE_CAPTION); }
{TABLE_ROW}       { LEXER_FLUSH(); TOKEN_APPEND(TABLE_ROW); }
{TABLE_HEADER}    { LEXER_FLUSH(); TOKEN_APPEND(TABLE_HEADER); }
{TABLE_DATA}      { LEXER_FLUSH(); TOKEN_APPEND(TABLE_DATA); }

{TAG_SOLO}        { LEXER_FLUSH(); TOKEN_APPEND(MARKUP); }
{TAG_OPEN}        { LEXER_FLUSH(); TOKEN_APPEND(MARKUP_OPEN); }
{TAG_CLOSE}       { LEXER_FLUSH(); TOKEN_APPEND(MARKUP_CLOSE); }

{RETURN}          { LEXER_FLUSH(); TOKEN_APPEND(RETURN); }
{SPACE}           { LEXER_FLUSH(); TOKEN_APPEND(SPACE); }
{NUMBER}          { LEXER_FLUSH(); TOKEN_APPEND(NUMBER); }
{PUNCT}           { LEXER_FLUSH(); TOKEN_APPEND(PUNCTUATION); }
{DASH}            { LEXER_FLUSH(); TOKEN_APPEND(DASH); }
{SQUOTE}          { LEXER_FLUSH(); TOKEN_APPEND(SQUOTE); }
{DQUOTE}          { LEXER_FLUSH(); TOKEN_APPEND(DQUOTE); }
{BRACKET}         { LEXER_FLUSH(); TOKEN_APPEND(BRACKET); }
{SYMBOL}          { LEXER_FLUSH(); TOKEN_APPEND(SYMBOL); }
{MONETARY}        { LEXER_FLUSH(); TOKEN_APPEND(MONETARY); }
{HTML_CHAR_NAMED} { LEXER_FLUSH(); TOKEN_APPEND(CHAR_HTML_NAMED); }
{HTML_CHAR_DEC}   { LEXER_FLUSH(); TOKEN_APPEND(CHAR_HTML_DEC); }
{HTML_CHAR_HEX}   { LEXER_FLUSH(); TOKEN_APPEND(CHAR_HTML_HEX); }

{TITLEDLINK}      { LEXER_FLUSH(); TOKEN_APPEND(OUTLINK); }
{URL}             { LEXER_FLUSH(); TOKEN_APPEND(URL); }

<INITIAL>{WIKILINK_OPEN}        { LEXER_FLUSH(); BEGIN(WIKILINK_COND); BLOCK_INIT(); }
<WIKILINK_COND>{WIKILINK_OPEN}  { BLOCK_ENTER(); }
<WIKILINK_COND>.                { BLOCK_CONTINUE(); }
<WIKILINK_COND>{RETURN}         { BLOCK_CONTINUE(); }
<WIKILINK_COND>{WIKILINK_CLOSE} { BLOCK_LEAVE(INLINK); }

<INITIAL>{TEMPLATE_OPEN}        { LEXER_FLUSH(); BEGIN(TEMPLATE_COND); BLOCK_INIT(); }
<TEMPLATE_COND>{TEMPLATE_OPEN}  { BLOCK_ENTER(); }
<TEMPLATE_COND>.                { BLOCK_CONTINUE(); }
<TEMPLATE_COND>{RETURN}         { BLOCK_CONTINUE(); }
<TEMPLATE_COND>{TEMPLATE_CLOSE} { BLOCK_LEAVE(TEMPLATE); }

<INITIAL>{COMMENT_OPEN}         { LEXER_FLUSH(); BEGIN(COMMENT_COND); BLOCK_INIT(); }
<COMMENT_COND>{COMMENT_OPEN}    { BLOCK_ENTER(); }
<COMMENT_COND>.                 { BLOCK_CONTINUE(); }
<COMMENT_COND>{RETURN}          { BLOCK_CONTINUE(); }
<COMMENT_COND>{COMMENT_CLOSE}   { BLOCK_LEAVE(COMMENT); }

.                 { LEXER_ACCUM(); }
<<EOF>>           { LEXER_FLUSH(); yyterminate(); }

%%
