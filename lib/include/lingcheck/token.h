/*
 * This file is part of the Linguistic Checker project.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#ifndef LINGCHECK_TOKEN_H
#define LINGCHECK_TOKEN_H

#include <string_view>

#include <cstring>

namespace lingcheck
{

namespace impl {

enum CLASS : int {
    CTEXT = 4,
    CCHAR,
    CMETA,
    CREF,
    CEXTRA,
    CSTYLE,
    CSCRIPT,
    CTAG,
    CTABLE,
    CHEADING
};

constexpr int make_class(CLASS c)
{
    return 1 << c;
}

} // namespace impl

enum TYPE : int {
    IGNORED      = -1,
    UNDEFINED    = 0,
    TEXT         = impl::make_class(impl::CTEXT),
    RETURN,
    SPACE,
    NUMBER,
    PUNCTUATION,
    DASH,
    SQUOTE,
    DQUOTE,
    BRACKET,
    SYMBOL,
    MONETARY,
    WORD,
    CHAR         = impl::make_class(impl::CCHAR),
    CHAR_HTML_NAMED,
    CHAR_HTML_DEC,
    CHAR_HTML_HEX,
    META         = impl::make_class(impl::CMETA),
    REFERENCE    = META + impl::make_class(impl::CREF),
    TEMPLATE,
    INLINK,
    OUTLINK,
    URL,
    EXTRA        = META + impl::make_class(impl::CEXTRA),
    MARKUP,
    MARKUP_OPEN,
    MARKUP_CLOSE,
    COMMENT,
    STYLE        = META + impl::make_class(impl::CSTYLE),
    ITALIC,
    BOLD,
    BOLD_ITALIC,
    STRIKETHROUGH,
    STYLE_OPEN,
    STYLE_CLOSE,
    SCRIPT       = META + impl::make_class(impl::CSCRIPT),
    SCRIPT_OPEN,
    SCRIPT_CLOSE,
    TAG          = META + impl::make_class(impl::CTAG),
    LIST,
    HRULE,
    TABLE        = TAG + impl::make_class(impl::CTABLE),
    TABLE_OPEN,
    TABLE_CLOSE,
    TABLE_CAPTION,
    TABLE_ROW,
    TABLE_HEADER,
    TABLE_DATA,
    HEADING      = TAG + impl::make_class(impl::CHEADING),
    H1,
    H2,
    H3,
    H4,
    H5,
    H6
};

class token
{
public:
    constexpr token() noexcept
        : token(nullptr, 0, UNDEFINED)
    {}

    constexpr token(const char* d) noexcept
        : token(d, ::strlen(d), UNDEFINED)
    {}

    constexpr token(const char* d, int s, TYPE t = UNDEFINED) noexcept
        : _data(d),
          _size(s),
          _type(t)
    {}

public:
    const char* data() const noexcept { return _data; }
    int size() const noexcept { return _size; }
    TYPE type() const noexcept { return _type; }

public:
    char operator[](size_t i) const noexcept { return _data[i]; }

public:
    operator bool() const noexcept { return (data() != nullptr) && (size() != 0); }

public:
    inline bool is(TYPE type) const noexcept
    {
        return _type == type;
    }

    inline bool is_nl() const noexcept
    {
        return is(RETURN);
    }

    inline bool is_list() const noexcept
    {
        return is(LIST);
    }

public:
    inline bool is_meta() const noexcept
    {
        return (_type & META) == META;
    }

    inline bool is_reference() const noexcept
    {
        return (_type & REFERENCE) == REFERENCE;
    }

    inline bool is_extra() const noexcept
    {
        return (_type & EXTRA) == EXTRA;
    }

    inline bool is_style() const noexcept
    {
        return (_type & STYLE) == STYLE;
    }

    inline bool is_script() const noexcept
    {
        return (_type & SCRIPT) == SCRIPT;
    }

    inline bool is_tag() const noexcept
    {
        return (_type & TAG) == TAG;
    }

    inline bool is_table() const noexcept
    {
        return (_type & TABLE) == TABLE;
    }

    inline bool is_title() const noexcept
    {
        return (_type & HEADING) == HEADING;
    }

public:
    inline bool operator==(const token& t) const noexcept
    {
        if (t.size() != size()) {
            return false;
        }

        if (t.data() == data()) {
            return true;
        }

        return std::string_view(data(), size()) == std::string_view(t.data(), t.size());
    }

    inline bool operator!=(const token& t) const noexcept
    {
        if (t.size() != size()) {
            return true;
        }

        if (t.data() == data()) {
            return false;
        }

        return std::string_view(data(), size()) != std::string_view(t.data(), t.size());
    }

    inline bool operator==(const char* str) const noexcept
    {
        return std::string_view(data(), size()) == str;
    }

    inline bool operator!=(const char* str) const noexcept
    {
        return std::string_view(data(), size()) != str;
    }

public:
    inline bool operator<(const token& t) const noexcept
    {
        return std::string_view(data(), size()) < std::string_view(t.data(), t.size());
    }

public:
    const char* type_name() const noexcept;

public:
    static const char* type_name(TYPE type) noexcept;

private:
    const char* _data;
    int _size;
    TYPE _type;
};

inline bool is_nl(const token& t) noexcept
{
    return t.is_nl();
}

inline bool is_meta(const token& t) noexcept
{
    return t.is_meta();
}

inline bool is_reference(const token& t) noexcept
{
    return t.is_reference();
}

inline bool is_extra(const token& t) noexcept
{
    return t.is_extra();
}

inline bool is_style(const token& t) noexcept
{
    return t.is_style();
}

inline bool is_script(const token& t) noexcept
{
    return t.is_script();
}

inline bool is_tag(const token& t) noexcept
{
    return t.is_tag();
}

inline bool is_table(const token& t) noexcept
{
    return t.is_table();
}

inline bool is_list(const token& t) noexcept
{
    return t.is_list();
}

inline bool is_title(const token& t) noexcept
{
    return t.is_title();
}

} // namespace lingcheck

#endif // LINGCHECK_TOKEN_H
