/*
 * This file is part of the Linguistic Checker project.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#ifndef LINGCHECK_FORMAT_H
#define LINGCHECK_FORMAT_H

#include <lingcheck/tokens.h>

#include <string>

namespace lingcheck
{

class format
{
public:
    format()
    {}

    virtual ~format() = default;

public:
    virtual void tokenize(std::string& text, tokens& tokens) const = 0;
    virtual size_t get_props(const token& token, tokens& props) const = 0;
};

} // lingcheck

#endif // LINGCHECK_FORMAT_H
