/* Copyright © 2024 Rémi Hérilier
 *
 * This file is part of the Linguistic Check project.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#ifndef LINGCHECK_UTILS_H
#define LINGCHECK_UTILS_H

#include <algorithm>
#include <string>

namespace lingcheck::utils {

/*****************************************************************************
 * std::string trimming got from
 * https://stackoverflow.com/questions/216823/how-to-trim-a-stdstring
 *****************************************************************************/

// trim from start (in place)
inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

inline void trim(std::string &s) {
    rtrim(s);
    ltrim(s);
}

} // namespace lingcheck::utils

#endif // LINGCHECK_UTILS_H
