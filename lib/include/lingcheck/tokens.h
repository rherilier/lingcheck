/* Copyright © 2024 Rémi Hérilier
 *
 * This file is part of the Linguistic Checker project.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#ifndef LINGCHECK_TOKENS_H
#define LINGCHECK_TOKENS_H

#include <lingcheck/token.h>

#include <vector>

namespace lingcheck
{

using tokens = std::vector<token>;

/**
 * increment @c i until the pointed token matchs the condition @c cond
 */
template <typename T, typename F>
bool move_until(const tokens& tokens, T& i, F&& cond)
{
    while ((i < tokens.size()) && (cond(tokens[i]) == false)) {
        ++i;
    }

    return (i < tokens.size());
}

/**
 * increment @c i until the pointed token is typed @c type
 */
template <typename T>
bool move_until(const tokens& tokens, T& i, TYPE type)
{
    while ((i < tokens.size()) && (tokens[i].type() != type)) {
        ++i;
    }

    return (i < tokens.size());
}

/**
 * increment @c i while the pointed token matchs the condition @c cond
 */
template <typename T, typename F>
bool move_while(const tokens& tokens, T& i, F&& cond)
{
    while ((i < tokens.size()) && cond(tokens[i])) {
        ++i;
    }

    return (i < tokens.size());
}

/**
 * increment @c i while the pointed token is typed @c type
 */
template <typename T>
bool move_while(const tokens& tokens, T& i, TYPE type)
{
    while ((i < tokens.size()) && (tokens[i].type() == type)) {
        ++i;
    }

    return (i < tokens.size());
}

template <typename T, typename F>
bool move_after(const tokens& tokens, T& i, F&& cond, bool expected)
{
    if (move_until(tokens, i, cond, expected)) {
        ++i;
    }

    return (i < tokens.size());
}

/**
 * increment @c i until the pointed token matchs the condition @c cond and move after it (when possible)
 */
template <typename T, typename F>
bool move_after(const tokens& tokens, T& i, F&& cond)
{
    if (move_until(tokens, i, cond)) {
        ++i;
    }

    return (i < tokens.size());
}

/**
 * increment @c i until pointed token is typed @c type and move after it (when possible)
 */
template <typename T, typename F>
bool move_after(const tokens& tokens, T& i, TYPE type)
{
    if (move_until(tokens, i, type)) {
        ++i;
    }

    return (i < tokens.size());
}

} // namespace lingcheck

#endif // LINGCHECK_TOKENS_H
