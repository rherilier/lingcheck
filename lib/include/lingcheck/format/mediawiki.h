/*
 * This file is part of the Linguistic Checker project.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#ifndef LINGCHECK_MEDIAWIKI_H
#define LINGCHECK_MEDIAWIKI_H

#include <lingcheck/format.h>

namespace lingcheck {

class mediawiki : public format
{
public:
    mediawiki()
    {}

public:
    void tokenize(std::string& text, tokens& tokens) const override;
    size_t get_props(const token& token, tokens& props) const override;
};

} // namespace lingcheck

#endif // LINGCHECK_MEDIAWIKI_H
