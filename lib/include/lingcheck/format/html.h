/*
 * This file is part of the Linguistic Checker project.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#ifndef LINGCHECK_HTML_H
#define LINGCHECK_HTML_H

#include <lingcheck/format.h>


namespace lingcheck {

class html : public format
{
public:
    html()
    {}

public:
    static bool get_utf8_from_named(const token& t, token& c);
    static bool is_named_space(const token& t);
};

} // namespace lingcheck

#endif // LINGCHECK_HTML_H
